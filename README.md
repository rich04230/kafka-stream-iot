## setting up your own project to write a stream processing application using Kafka Streams

### Demo WordCount app

- prepare input and output topic
```bash
bin/kafka-topics.sh --create \
    --bootstrap-server localhost:9092 \
    --replication-factor 1 \
    --partitions 1 \
    --topic streams-plaintext-input
    
bin/kafka-topics.sh --create \
    --bootstrap-server localhost:9092 \
    --replication-factor 1 \
    --partitions 1 \
    --topic streams-wordcount-output \
    --config cleanup.policy=compact
```

- start the Wordcount Application
```bash
bin/kafka-run-class.sh org.apache.kafka.streams.examples.wordcount.WordCountDemo
```

- start the console consumer in a separate terminal to read output data
```bash
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 \
    --topic streams-wordcount-output \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
```

- write some message with the console producer into the input topic streams-plaintext-input
```bash
> bin/kafka-console-producer.sh --broker-list localhost:9092 --topic streams-plaintext-input
all streams lead to kafka
```

### Create Streams app

- use a Kafka Streams Maven Archetype for creating a Streams project structure
```bash
mvn archetype:generate \
    -DarchetypeGroupId=org.apache.kafka \
    -DarchetypeArtifactId=streams-quickstart-java \
    -DarchetypeVersion=2.4.1 \
    -DgroupId=com.rich.stream \
    -DartifactId=stream-app \
    -Dversion=0.1 \
    -Dpackage=com.rich.stream \
    -DinteractiveMode=false
```

- libraries

| Group ID         | Artifact ID         | Version | Description                                                                               |
|------------------|---------------------|---------|-------------------------------------------------------------------------------------------|
| org.apache.kafka | kafka-streams       | 2.4.1   | Base library for Kafka Streams.                                                           |
| org.apache.kafka | kafka-clients       | 2.4.1   | Kafka client library. Contains built-in serializers/deserializers.                        |
| org.apache.kafka | kafka-streams-scala | 2.4.1   | (Optional) Kafka Streams DSL for Scala library to write Scala Kafka Streams applications. |
