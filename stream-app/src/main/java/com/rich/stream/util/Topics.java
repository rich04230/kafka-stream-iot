package com.rich.stream.util;

public class Topics {
    public static final String DEVICE_TEMPERATURE = "streaming.iot.temperature";
    public static final String DEVICE_TEMPERATURE_ABNORMAL = "streaming.iot.temperature.abnormal";
    public static final String DEVICE = "streaming.iot.device";
    public static final String DEVICE_DETAIL = "streaming.iot.device.detail";
}
