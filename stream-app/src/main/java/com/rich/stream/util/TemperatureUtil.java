package com.rich.stream.util;

import java.math.BigDecimal;

public class TemperatureUtil {

    public static final String CELSIUS_SYMBOL = "C";
    public static final String FAHRENHEIT_SYMBOL = "F";
    public static final BigDecimal CELSIUS_THRESHOLD = new BigDecimal("30");
    public static final BigDecimal FAHRENHEIT_THRESHOLD = new BigDecimal("105");

    public static double toFahrenheit(String temperature) {
        if(temperature == null && temperature.equals("")) {
            return 0;
        }
        String temperatureTrim = temperature.trim().replace("F", "");
        return Double.parseDouble(temperatureTrim);
    }

    public static double fahrenheitToCelsius(double fahrenheit) {
        return new BigDecimal((( 5 *(fahrenheit - 32.0)) / 9.0))
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static String fahrenheitToCelsius(String fahrenheit) {
        return new BigDecimal((( 5 *(toFahrenheit(fahrenheit) - 32.0)) / 9.0))
                .setScale(2, BigDecimal.ROUND_HALF_UP).toString() + CELSIUS_SYMBOL;
    }

    public static boolean isAbnormalFahrenheit(double fahrenheit) {
        return new BigDecimal(fahrenheit).compareTo(FAHRENHEIT_THRESHOLD) == 1;
    }

    public static boolean isTemperatureAbnormal(double temperatureC) {
        return new BigDecimal(temperatureC).compareTo(CELSIUS_THRESHOLD) == 1;
    }
}
