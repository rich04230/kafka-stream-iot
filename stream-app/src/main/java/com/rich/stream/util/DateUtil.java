package com.rich.stream.util;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    public static String format(long timestampMilli) {
        return ZonedDateTime.ofInstant(Instant.ofEpochMilli(timestampMilli), ZoneId.systemDefault()).format(FORMATTER);
    }
}
