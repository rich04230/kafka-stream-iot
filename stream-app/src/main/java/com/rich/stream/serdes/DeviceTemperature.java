package com.rich.stream.serdes;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceTemperature {

    private long id;
    private String name;
    private String type;
    private String temperature;
    private long timestamp;
    private String datetime;

}
