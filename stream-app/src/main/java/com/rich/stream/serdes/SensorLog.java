package com.rich.stream.serdes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SensorLog {
    private long id;
    private String temperature;
    private long timestamp;
}
