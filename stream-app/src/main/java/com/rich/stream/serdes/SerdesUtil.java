package com.rich.stream.serdes;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

public class SerdesUtil {

    public static Serde<SensorLog> getSensorLogSerdes() {
        JsonSerializer<SensorLog> logJsonSerializer = new JsonSerializer<>();
        JsonDeserializer<SensorLog> logJsonDeserializer = new JsonDeserializer<>(SensorLog.class);
        return Serdes.serdeFrom(logJsonSerializer, logJsonDeserializer);
    }

    public static Serde<DeviceInfo> getDeviceInfoSerdes() {
        JsonSerializer<DeviceInfo> deviceInfoJsonSerializer = new JsonSerializer<>();
        JsonDeserializer<DeviceInfo> deviceInfoJsonDeserializer = new JsonDeserializer<>(DeviceInfo.class);
        return Serdes.serdeFrom(deviceInfoJsonSerializer, deviceInfoJsonDeserializer);
    }

    public static Serde<DeviceTemperature> getDeviceTemperatureSerdes() {
        JsonSerializer<DeviceTemperature> deviceTempSerializer = new JsonSerializer<>();
        JsonDeserializer<DeviceTemperature> deviceTempDeserializer = new JsonDeserializer<>(DeviceTemperature.class);
        return Serdes.serdeFrom(deviceTempSerializer, deviceTempDeserializer);
    }
}
