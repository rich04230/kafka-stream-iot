package com.rich.stream.serdes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeviceInfo {

    private long id;
    private String name;
    private String type;
//    private double lat;
//    private double long;


}
