package com.rich.stream.processor;

import com.rich.stream.serdes.SensorLog;
import com.rich.stream.serdes.SerdesUtil;
import com.rich.stream.util.TemperatureUtil;
import com.rich.stream.util.Topics;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class DeviceFilterApp {

    public static void main(String[] args) throws Exception {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "device-streams-filter-6");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        final StreamsBuilder builder = new StreamsBuilder();

        final KStream<String, SensorLog> logStream = builder.stream(Topics.DEVICE_TEMPERATURE,
                Consumed.with(Serdes.String(), SerdesUtil.getSensorLogSerdes()));
//        logStream.print(Printed.toSysOut());
// stateless transformation, or use visualization tool: https://github.com/zz85/kafka-streams-viz

        logStream
                .filter((key, sensorLog) -> isAbnormal(sensorLog))
                .mapValues(sensorLog -> {
                    sensorLog.setTemperature(TemperatureUtil.fahrenheitToCelsius(sensorLog.getTemperature()));
                    return sensorLog;
                })
                .to(Topics.DEVICE_TEMPERATURE_ABNORMAL, Produced.with(Serdes.String(), SerdesUtil.getSensorLogSerdes()));


        final Topology topology = builder.build();
        // describe the topology
        log.info("topology: {}", topology.describe());

        startStream(props, topology);
    }

    private static void startStream(Properties props, Topology topology) {
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }

    public static boolean isAbnormal(SensorLog sensorLog) {
        double fahrenheit = TemperatureUtil.toFahrenheit(sensorLog.getTemperature());
        return TemperatureUtil.isAbnormalFahrenheit(fahrenheit);
    }

    public static void print(String key, SensorLog sensorLog) {
        log.info("Key: {}, value: {}", key, sensorLog);
    }
}

