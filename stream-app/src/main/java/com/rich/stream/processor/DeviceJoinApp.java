package com.rich.stream.processor;

import com.rich.stream.serdes.DeviceInfo;
import com.rich.stream.serdes.DeviceTemperature;
import com.rich.stream.serdes.SensorLog;
import com.rich.stream.serdes.SerdesUtil;
import com.rich.stream.util.DateUtil;
import com.rich.stream.util.Topics;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class DeviceJoinApp {

    public static void main(String[] args) throws Exception {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "device-streams-app-3");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        final StreamsBuilder builder = new StreamsBuilder();

        final KTable<String, DeviceInfo> deviceInfoTable = builder.table(Topics.DEVICE,
                Consumed.with(Serdes.String(), SerdesUtil.getDeviceInfoSerdes()));
        deviceInfoTable.toStream().print(Printed.toSysOut());

        final KStream<String, SensorLog> sensorLogStream = builder.stream(Topics.DEVICE_TEMPERATURE,
                Consumed.with(Serdes.String(), SerdesUtil.getSensorLogSerdes()));
//        sensorLogStream.print(Printed.toSysOut());

//        final KStream<String, SensorLog> deviceIdLogStream = sensorLogStream
//                .map((key, log) -> new KeyValue<>(Long.toString(log.getDeviceId()), log));
//        deviceIdLogStream.print(Printed.toSysOut());


        sensorLogStream.leftJoin(deviceInfoTable, (sensorLog, device) -> {
            log.debug("sensorLog: {}, device: {}", sensorLog, device);
            if(device == null) {
                return DeviceTemperature.builder().build();
            }
            return DeviceTemperature.builder()
                    .id(device.getId())
                    .name(device.getName())
                    .type(device.getType())
                    .timestamp(sensorLog.getTimestamp())
                    .datetime(DateUtil.format(sensorLog.getTimestamp()))
                    .temperature(sensorLog.getTemperature())
                    .build();
        })
                .map(KeyValue::new)
                .to(Topics.DEVICE_DETAIL, Produced.with(Serdes.String(), SerdesUtil.getDeviceTemperatureSerdes()));
//        .print(Printed.toSysOut());

        final Topology topology = builder.build();
        // describe the topology
        log.info("topology: {}", topology.describe());

        startStream(props, topology);
    }

    private static void startStream(Properties props, Topology topology) {
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
